" Commands:
" :Man ~ Search man pages within vim
" :Shell ~ Outputs of shell command in a new window

" Quite Important: ----------------------
" I use the <localleader> to replace the leader usage in all file-specific
" commands. So basically all commands which are set with autocommand (au) will
" be utilizing localleader. An example of this is <localleader>r which is
" usually used to run files.
"
" I've defaulted it to the same value as leader, but I recommend changing this
" to "\<space>" in order to mentally separate the two. I'm not your mom
" though, so you do you.
let maplocalleader = "\\"

nnoremap <localleader>ev :edit ~/.vimrc<cr>

" Playground: {{{
"
" }}} Playground

" Plugins (Vundle): {{{

" Required for Vundle.vim
set nocompatible  " Make it VIM, not VI
filetype off  " Turn off filetype detection for a bit

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" Plugins:
Plugin 'gmarik/Vundle.vim'
Plugin 'morhetz/gruvbox'
Plugin 'tpope/vim-surround'
Plugin 'scrooloose/nerdtree'
Plugin 'godlygeek/tabular'
Plugin 'SirVer/ultisnips'
Plugin 'honza/vim-snippets'
Plugin 'kien/ctrlp.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'moll/vim-bbye'
Plugin 'scrooloose/nerdcommenter'
Plugin 'jiangmiao/auto-pairs'
Plugin 'tpope/vim-repeat'
Plugin 'triglav/vim-visual-increment'
Plugin 'tmhedberg/SimpylFold'
Plugin 'majutsushi/tagbar'
"Plugin 'beloglazov/vim-online-thesaurus'
Plugin 'pangloss/vim-javascript'
"Plugin 'mxw/vim-jsx'
Plugin 'nelstrom/vim-markdown-folding'
"Plugin 'davidhalter/jedi-vim'
Plugin 'justinmk/vim-syntax-extra'
"Plugin 'ervandew/supertab'
Plugin 'sheerun/vim-polyglot'
Plugin 'fatih/vim-go'
"Plugin 'w0rp/ale'
Plugin 'itchyny/lightline.vim'
Plugin 'ap/vim-buftabline'
Plugin 'airblade/vim-gitgutter'
"Plugin 'jpalardy/vim-slime'
Plugin 'Valloric/YouCompleteMe'
Plugin 'file:///home/jmend/Code/vim-ngraph'

" Man page in VIM
runtime! ftplugin/man.vim

call vundle#end()
filetype plugin indent on
" }}} Plugins (Vundle)

" General Options: {{{
set t_Co=256
set t_ut=

" Sets the shell to use
set shell=/bin/bash

" Allows you to define a .vimrc or .exrc in the CWD
" while disallowing the use of :autocmd in that rc
set exrc
set secure

" Specifies the behavior of buffers when they are sent to an `inactive` state.
" An `inactive` state is one in which there is no window displaying that
" buffer. The 'bufhidden' option allows defining what action to take when a
" buffer is about to be hidden. Using `bufhidden=delete` allows a buffer to be
" removed once the window is closed OR used for something else. See
" windows.txt for more information about the nuances between 'hidden' and the
" various window-modifying functions
set hidden

" Tabs
set tabstop=2  " col-size of tabs
set shiftwidth=2  " sets what >> and << ops do
set expandtab  " replace tabs with spaces
set smarttab  " More reasonable tab actions

" Reasonable backspace functionality
set backspace=indent,eol,start

" Replace certain characters visually
set listchars=tab:\>\ ,trail:·
set list

" Line number things
set number
set numberwidth=4
set relativenumber

" Show info
set ruler
set showcmd
set statusline=%f\ %=L:%l/%L\ %c\ (%p%%)

" Tab completion for : command
set wildmenu
set wildmode=longest,list,full

" Highlight search + incremental search
set hlsearch
set incsearch

" Ignore case, unless you use uppercase characters
set ignorecase
set smartcase

" Other
set fileencodings=utf-8
set tags=./tags
set printoptions=number:y,duplex:long,paper:letter
set clipboard=unnamedplus
set errorbells
set laststatus=2
set cursorline

" Themes
colorscheme gruvbox
syntax enable
set bg=dark
" }}} General Settings

" Keymappings and FT-Specific Settings: {{{


" Moves around a line more closely to what is expected (at least by me) when
" the line is wrapped.
nnoremap j gj
nnoremap k gk
vnoremap j gj
vnoremap k gk

" Moving around between windows quickly
noremap <C-j> <C-W>j
noremap <C-k> <C-W>k
noremap <C-h> <C-W>h
noremap <C-l> <C-W>l

" Useful commands for editing the vimrc
nnoremap <silent> <leader>ve :vsplit $MYVIMRC<cr>
nnoremap <silent> <leader>vs :source $MYVIMRC<cr>

" Mappings for selection of next set of brackets
function! s:NextSetOf(parentype, modifier, search)
    execute "normal! " . a:search . a:parentype . "\r:nohlsearch\r"
    execute "normal! v" . a:modifier . a:parentype
endfunction

" Defines [ia][nN][()]
function! s:DefineNextSetOf(parens)
    for p in split(a:parens, '\zs')
        for m in split("ia", '\zs')
            for d in split("nN", '\zs')
                for t in split("ov", '\zs')
                    let l:s = (d ==# 'n') ? '/' : '?'
                    let l:map = t . "noremap"
                    let l:op = m . d . p
                    let l:cmd = ":\<c-u>call <SID>NextSetOf('" . p . "', '" . m . "', '" . l:s . "')\<cr>"
                    execute join([l:map, l:op, l:cmd], ' ')
                endfor
            endfor
        endfor
    endfor
endfunction

call s:DefineNextSetOf("(){}[]<>")

" Autocommands are split into filetype `augroup`s, each is separated by
" filetype. This solves the problem of sourcing the vimrc multiple times
" causing multiple duplicated autocommands to be set. An augroup is only run
" once**.
"
" These keymappings depend on the filetype, when :filetype on is enabled (as
" it is earlier in this config), when vim first loads a buffer, it will
" automatically detect the filetype and set the 'filetype' option (buffer)
" locally. After this happens, any `FileType` type autocommands are triggered
"
" NOTES:
" ** An augroup doesn't provide this functionality by itself. When you
" redefine it, it will 'add onto' the original one, in order to clear one, you
" can add `autocommand!` or `au!` to it (or another with the same name). This
" is used to make sure that only one version of the autocommand hooks is set
" per buffer.
augroup ft_latex
    autocmd!
    autocmd FileType tex setlocal nocursorline
    autocmd FileType tex setlocal tabstop=2 shiftwidth=2
    autocmd FileType tex nnoremap <buffer> <localleader>r
                \ :!pdflatex -halt-on-error %<cr>
augroup END

augroup ft_c
    autocmd!
    autocmd FileType c setlocal tabstop=2 shiftwidth=2
    autocmd FileType c nnoremap <buffer> <localleader>r
                \ :make<CR>
    autocmd FileType c nnoremap <buffer> <localleader>R
                \ :!clang -Wall -O3 -o a.out %;./a.out;rm a.out<cr>
    autocmd FileType c nnoremap <buffer> <localleader>d
                \ :YcmCompleter GoTo<cr>
augroup END


augroup ft_cc
    autocmd!
    autocmd FileType cpp setlocal tabstop=2 shiftwidth=2  " Will target all cpp/cc/h files
    " TODO: fix this, jank as fuck
    "autocmd BufWritePre,FileWritePre *.cc :!clang-format -style=Google -i %:p:t
    "autocmd BufWritePre,FileWritePre *.h :!clang-format -style=Google -i %:p:t
    autocmd FileType cpp nnoremap <buffer> <localleader>r
                \ :call <SID>BuildCpp()<cr>
    autocmd FileType cpp nnoremap <buffer> <localleader>R
                \ :make<CR>
    autocmd FileType cpp nnoremap <buffer> <localleader>d
                \ :YcmCompleter GoTo<cr>

    command! -buffer -nargs=* RunCpp call <SID>CallBuildCpp(<q-args>)
augroup END

augroup ft_python
    autocmd!
    autocmd FileType python nnoremap <buffer> <localleader>r
                \ :call <SID>StartTerminal('python ' . expand('%'))<cr>
augroup END

augroup ft_scheme
    autocmd!
    autocmd FileType scheme nnoremap <buffer> <localleader>r
                \ :w<CR> :!mit-scheme --load % <CR>
    autocmd FileType scheme setlocal colorcolumn=79
augroup END

augroup ft_java
    autocmd!
    autocmd FileType java nnoremap <silent> <buffer> <localleader>r
          \ :silent call <SID>BuildAndMaybeRunJava(expand('%:p'), expand('%:r'))<cr>
    autocmd FileType java
          \ setlocal tabstop=2 softtabstop=2 tabstop=2 smarttab
    autocmd FileType java
          \ command! -buffer -nargs=? Run
          \ call <SID>BuildAndMaybeRunJava(
          \ expand('%:p'),
          \ strlen(<q-args>) ? <q-args> : expand('%:r'))
    autocmd FileType java
          \ command! -buffer -nargs=0 Class
          \ !fzf < /home/jmend/.vim/classnamesdb
augroup END

function! s:BuildAndMaybeRunJava(entrypoint, class)
  let l:origcur = getcurpos()
  " Create tmp directory for build files
  python3 vim.vars["random"] = __import__("random").randint(1000, 100000)
  let l:random = get(g:, 'random', 0)
  let l:jdir = '/tmp/java-' . l:random
  echom "Writing output to: " . l:jdir
  call mkdir(l:jdir)

  " Save winid
  let l:winid = win_getid(winnr())

  " Open (maybe) new window
  let l:bufnr = <SID>MakeScratchWindow('')
  set bufhidden=delete
  set nobuflisted
  set nocursorline
  set norelativenumber

  let l:BaseOpts = { -> {
        \   'out_io': 'buffer', 'out_buf': l:bufnr,
        \   'err_io': 'buffer', 'err_buf': l:bufnr,
        \ }}

  let l:RunCmd = {cmd, opts -> job_start(
        \   cmd, extend(l:BaseOpts(), opts))}

  let l:compile_cmd = 'javac -d ' . l:jdir . ' ' . a:entrypoint
  let l:run_cmd = 'java -cp ' . l:jdir . ' ' . a:class

  call l:RunCmd(
        \   l:compile_cmd,
        \   {'exit_cb': {_, s -> s ? 1 : l:RunCmd(l:run_cmd, {
        \     'exit_cb': { -> execute('echom "Done!"', '')},
        \   })}}
        \ )


  "let l:output = systemlist('javac -d ' . l:jdir . ' ' . a:entrypoint)
  "if v:shell_error == 0
    "" Build succeeded, run class
    "let l:output = systemlist('java -cp ' . l:jdir . ' ' . a:class)
  "end
  "" call append(0, substitute(l:output, '\%x00', '', 'g'))
  "call append(0, l:output)
  "normal dd
  call win_gotoid(l:winid)
  call setpos('.', l:origcur)
endfunction

augroup ft_javascript
    autocmd!
    " Note: Depending on whether the vim-jsx plugin is installed, this may
    " need to be changed to filter on the `javascript` filetype
    autocmd FileType javascript nnoremap <buffer> <localleader>r
          \ :w<CR> :!node %<CR>
    autocmd FileType javascript nnoremap <buffer> <localleader>R
          \ :w<CR> :SyntasticCheck<CR>
    autocmd FileType javascript
          \ setlocal tabstop=2 softtabstop=2 tabstop=2 smarttab
    autocmd FileType javascript nnoremap <buffer> <localleader>d
          \ :YcmCompleter GoTo<CR>
augroup END

augroup ft_markdown
    autocmd!
    autocmd FileType markdown nnoremap <buffer> ]h :<c-u>call <SID>MarkdownGoToHeader(v:true)<cr>
    autocmd FileType markdown nnoremap <buffer> [h :<c-u>call <SID>MarkdownGoToHeader(v:false)<cr>
    autocmd FileType markdown highlight htmlH1 ctermfg=DarkRed
    autocmd FileType markdown highlight htmlH2 ctermfg=DarkCyan
    autocmd FileType markdown highlight htmlH3 ctermfg=DarkGreen
    autocmd FileType markdown highlight htmlH4 ctermfg=LightGray
    autocmd FileType markdown nnoremap <buffer> <leader>r
                \ :!pandoc %:p -s --highlight-style kate --pdf-engine=xelatex -o gen/%:t:r.pdf<cr>
augroup END

augroup ft_vim
    autocmd!
    autocmd FileType vim setlocal foldmethod=marker
    autocmd FileType vim nnoremap <buffer> <localleader>r
          \ :source %<cr>
augroup END

augroup ft_dis
    autocmd!
    autocmd FileType dis nnoremap <buffer> <localleader>f
                \ :call <SID>DisGoTo()<cr>
augroup END

" Use quickfix window when using :make
augroup cfg_quickfix_fix
    autocmd QuickFixCmdPost [^l]* nested cwindow
    autocmd QuickFixCmdPost    l* nested lwindow
augroup end

nnoremap <silent> <leader>w :nohlsearch<Bar>:echo<cr>

nnoremap <space> za
vnoremap <space> zf

" Set filetypes
nnoremap <leader>td :set filetype=dis<cr>

nnoremap <leader>q :Bdelete<cr>
nnoremap <leader>' :NERDTreeToggle<cr>
noremap <F12> :setlocal spell! spelllang=en_us<cr>
noremap <F11> :syntax spell toplevel<cr>
nnoremap <leader><tab> :TagbarToggle<cr>
" }}} Keymappings and FT-Specific Settings

" Plugin Settings: {{{
"" make YCM compatible with UltiSnips (using supertab)
let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
"let g:SuperTabDefaultCompletionType = '<C-n>'

" better key bindings for UltiSnipsExpandTrigger
let g:UltiSnipsExpandTrigger = "<tab>"
let g:UltiSnipsJumpForwardTrigger = "<tab>"
let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"

" Taken from
" https://github.com/statico/dotfiles/blob/202e30b23e5216ffb6526cce66a0ef4fa7070456/.vim/vimrc#L406-L453
let g:lightline = {
\ 'active': {
\   'left': [['mode', 'paste'], ['filename', 'modified']],
\   'right': [['lineinfo'], ['percent'], ['readonly', 'linter_warnings', 'linter_errors', 'linter_ok']]
\ },
\ 'component_expand': {
\   'linter_warnings': 'LightlineLinterWarnings',
\   'linter_errors': 'LightlineLinterErrors',
\   'linter_ok': 'LightlineLinterOK'
\ },
\ 'component_type': {
\   'readonly': 'error',
\   'linter_warnings': 'warning',
\   'linter_errors': 'error'
\ },
\ }

function! LightlineLinterWarnings() abort
  let l:counts = ale#statusline#Count(bufnr(''))
  let l:all_errors = l:counts.error + l:counts.style_error
  let l:all_non_errors = l:counts.total - l:all_errors
  return l:counts.total == 0 ? '' : printf('%d ◆', all_non_errors)
endfunction

function! LightlineLinterErrors() abort
  let l:counts = ale#statusline#Count(bufnr(''))
  let l:all_errors = l:counts.error + l:counts.style_error
  let l:all_non_errors = l:counts.total - l:all_errors
  return l:counts.total == 0 ? '' : printf('%d ✗', all_errors)
endfunction

function! LightlineLinterOK() abort
  let l:counts = ale#statusline#Count(bufnr(''))
  let l:all_errors = l:counts.error + l:counts.style_error
  let l:all_non_errors = l:counts.total - l:all_errors
  return l:counts.total == 0 ? '✓ ' : ''
endfunction

augroup cfg_ale_lint
    autocmd User ALELint call lightline#update()
augroup END

let g:ale_pattern_options = {'\.js$': {'ale_enabled': 0}}

let g:slime_target = "tmux"

" Online Thesaurus
let g:online_thesaurus_map_keys = 0

let g:polyglot_disabled = ['tex', 'latex', 'c', 'python']
let g:vim_markdown_new_list_item_indent = 0
let g:vim_markdown_folding_disabled = 1

" BufTabLine
let g:buftabline_numbers = 2  " use ordinal numbers (2) rather than bufnum (1)
let g:buftabline_indicators = v:true
let g:buftabline_separators = v:false
nmap <leader>1 <Plug>BufTabLine.Go(1)
nmap <leader>2 <Plug>BufTabLine.Go(2)
nmap <leader>3 <Plug>BufTabLine.Go(3)
nmap <leader>4 <Plug>BufTabLine.Go(4)
nmap <leader>5 <Plug>BufTabLine.Go(5)
nmap <leader>6 <Plug>BufTabLine.Go(6)
nmap <leader>7 <Plug>BufTabLine.Go(7)
nmap <leader>8 <Plug>BufTabLine.Go(8)
nmap <leader>9 <Plug>BufTabLine.Go(9)
nmap <leader>0 <Plug>BufTabLine.Go(10)

let g:netre_liststyle=3

let g:ctrlp_custom_ignore = {
    \ 'dir':  '\.git$\|\.hg$\|\.svn$\|bower_components$\|dist$\|node_modules$\|project_files$\|test$\|env$\|bower_modules$',
    \ 'file': '\.exe$\|\.so$\|\.dll$\|\.pyc$' }

let g:tex_flavor='latex'

let g:slime_target = "tmux"

set rtp+=~/.vim/mySnippets
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
let g:UltiSnipsEditSplit="vertical"
let g:UltiSnipsSnippetsDir="~/.vim/mySnippets"
let g:UltiSnipsSnippetDirectories=["UltiSnips", "mySnippets"]

let g:ale_fixers = {
            \   'python': [
            \     'autopep8',
            \   ]
            \ }

let g:ale_linters = {
            \ 'cpp': ['clang', 'clangcheck', 'cppcheck', 'cpplint', 'g++']
            \ }

let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'

let g:jsx_ext_required = 0

let g:gitgutter_sign_added = '··'
let g:gitgutter_sign_modified = '··'
let g:gitgutter_sign_removed = '·'
let g:gitgutter_sign_modified_removed = '·'

"let g:SuperTabDefaultCompletionType = "<c-x><c-o>"
let g:ycm_python_binary_path = '/usr/bin/python'
" }}} Plugin Settings

" Functions: {{{
" The Silver Searcher
if executable('ag')
    " Use ag over grep
    set grepprg=ag\ --nogroup\ --nocolor

    " Use ag in CtrlP
    let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'

    " ag is fast enough that CtrlP doesn't need to cache
    let g:ctrlp_use_caching = 0
endif

nnoremap K :grep! "\b<C-R><C-W>\b"<CR>:cw<CR>

" Adds the :Shell function
" Works the same as :! but instead of opening shell, it opens a new scratch
" window with the shell output :)
command! -complete=shellcmd -nargs=+ Shell call s:RunShellCommand(<q-args>)
function! s:RunShellCommand(cmdline)
  echo a:cmdline
  let expanded_cmdline = a:cmdline
  for part in split(a:cmdline, ' ')
     if part[0] =~ '\v[%#<]'
        let expanded_part = fnameescape(expand(part))
        let expanded_cmdline = substitute(expanded_cmdline, part, expanded_part, '')
     endif
  endfor
  botright new
  setlocal buftype=nofile bufhidden=wipe nobuflisted noswapfile nowrap
  "call setline(1, 'You entered:    ' . a:cmdline)
  "call setline(2, 'Expanded Form:  ' .expanded_cmdline)
  "call setline(3,substitute(getline(2),'.','=','g'))
  call setline(1, 'Executed: ' . a:cmdline)
  call setline(2, substitute(getline(1),'.','=','g'))
  execute '$read !'. expanded_cmdline
  setlocal nomodifiable
  1
endfunction

function! s:DisGoTo()
    " Jump to address, using tabs as markers
    execute "normal! 0/\t\rnww"
    " Extract address
    let l:address = expand("<cword>")
    " Search for the address in the object-file
    execute 'normal! /^\s\+' . l:address . ":\r"
endfunction

function! s:MarkdownGoToHeader(forward)
    let l:opts = (a:forward ? '' : 'b')
                \ . 'W' " don't wrap around the end of the file
                \ . 'z' " start searching at cursor position rather tha column 0
    let l:didwork =  search('\v^#+ ', l:opts)
    call assert_notequal(l:didwork, 0)
endfunction

function! s:MakeScratchWindow(name)
  let l:last_bufwinid = get(g:, "jmi_last_bufwinid", -1)
  let l:window_size = get(g:, "jm_make_window_size", 10)

  let l:last_winnr = win_id2win(l:last_bufwinid)
  if l:last_winnr != 0
    execute l:last_winnr . "wincmd q"
  endif

  execute "botright " . l:window_size . "new"
  setlocal buftype=nofile
  setlocal bufhidden=hide
  setlocal noswapfile
  let l:bufnr = bufnr('%')
  if len(a:name) != 0
    execute "file! " . name
  endif

  let g:jmi_last_bufwinid = bufwinid(l:bufnr)
  return l:bufnr
endfunction

function! s:StartTerminal(command)
  let l:bufnum = <SID>MakeScratchWindow('')
  if l:bufnum != bufnr('%')
    call setpos('.', [l:bufnum, 0, 0, 0])
  endif
  let l:options = {
        \   "curwin": v:true
        \ }
  call term_start(a:command, l:options)
endfunction

function! s:RunCommand(command)
  let l:output = systemlist(a:command)
  if (len(l:output) != 0)
    let l:bufnum = <SID>MakeScratchWindow('')
    call appendbufline(l:bufnum, 0, l:output)
  else
    echo "~~ No Output ~~"
  endif
  return l:output
endfunction

function! s:BuildCpp()
  let l:cxx_flags = get(g:, 'jm_cxx_flags', '-Wall -Werror -pedantic -g -O2')
  let l:flags = get(g:, 'jm_bin_flags', '')
  let l:cc = "clang++"
  let l:filename = expand('%:p')
  let l:filename_output = expand('%:p:r')
  let l:output = '-o ' . l:filename_output
  let l:command = join([l:cc, l:cxx_flags, l:filename, l:output], ' ')
  let l:output = <SID>RunCommand(command)
  if len(l:output) == 0
    if len(l:flags) == 0
      call <SID>StartTerminal(l:filename_output)
    else
      call <SID>StartTerminal(l:filename_output . ' ' . l:flags)
    end
  endif
endfunction

function! s:CallBuildCpp(all_flags)
  let l:args = split(a:all_flags, ' -- ')

  if len(l:args) == 1
    let g:jm_cxx_flags = l:args[0]
    let g:jm_bin_flags = ''
  elseif len(l:args) == 2
    let [g:jm_cxx_flags, g:jm_bin_flags] = l:args
  else
    let g:jm_cxx_flags = ''
    let g:jm_bin_flags = ''
  endif

  call <SID>BuildCpp()
endfunction

" }}} Functions
